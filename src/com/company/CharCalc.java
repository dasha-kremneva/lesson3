package com.company;

import java.util.HashMap;
import java.util.Map;

public class CharCalc {

    public void countCharacters(String text) throws Exception {
        Map<Character, Integer> myMap = new HashMap<>();
        if (text == null|| text.isEmpty())
            throw new Exception("Ошибка: отсутствует текст");

        for (int i = 0; i < text.length(); i++) {
            char c = text.toUpperCase().charAt(i);
            if (Character.isLetter(c)) {
                if (myMap.containsKey(c)) {
                    myMap.put(c, myMap.get(c) + 1);
                } else {
                    myMap.put(c, 1);
                    //System.out.println(myMap.get(i));
                }
            }
        }

        //  сортировка + вывод
        myMap.entrySet().stream()
                .sorted(Map.Entry.<Character, Integer>comparingByValue().reversed())
                .forEach(System.out::println);
    }
}
