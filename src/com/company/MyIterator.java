package com.company;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyIterator implements Iterable<String> {

    // Массив по которому итерируемся
    private final String[] arr;

    public MyIterator(String[] arr) {
        this.arr = arr;
    }

    @Override
    public Iterator iterator() {
        return new ImpIterator();
    }

    // Класс ImpIterator, имплементирующий интерфейс Iterator
    private class ImpIterator implements Iterator<String> {
        int current = 0;

        @Override
        public boolean hasNext() {
            return current != arr.length;
        }

        @Override
        public String next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            return arr[current++];
        }
    }
}
