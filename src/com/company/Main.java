package com.company;

import java.util.List;
import java.util.Scanner;

import static com.company.Ideone.calc;

public class Main {

    public static void main(String[] args) {
        // ЗАДАНИЕ 1 - ДЕМОНСТРАЦИЯ
//        String text = "Никогда не сдаваться... Встать, когда все рухнуло — вот настоящая сила. (Наруто Удзумаки)";
//        String[] arr = text.split(" ");
//        MyIterator myIterator = new MyIterator(arr);
//        for (String s : arr)
//            System.out.print(s + " ");

        // ЗАДАНИЕ 2 - ДЕМОНСТРАЦИЯ
//        Scanner in = new Scanner(System.in);
//        String s = in.nextLine();
//        PolishConverter n = new PolishConverter();
//        List<String> expression = n.parse(s);
//        boolean flag = n.flag;
//        if (flag) {
//            for (String x : expression) System.out.print(x + " ");
//            System.out.println();
//            System.out.println(calc(expression));
//        }

        // ЗАДАНИЕ 3 - ДЕМОНСТРАЦИЯ
        String text ="В мире шиноби тех, кто не подчиняется правилам, называют мусором," +
                " но те, кто не заботится о своих товарищах, куда хуже мусора..." +
                " я не хочу быть этим мусором. (Учиха Обито)";
        try {
            new CharCalc().countCharacters(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
